import os
import shutil 
import numpy as np
import torch 
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable 
from tqdm import tqdm 
import random
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt

random.seed(2020)

# Cuda related stuff.
try:
    use_cuda = torch.cuda.is_available()
except:
    use_cuda = False

if use_cuda:
    print('---------------------------')
    print('use_cuda = True')
    print('number of devices', torch.cuda.device_count())
    device = torch.cuda.current_device()
    print('device index: ', device)
    print('device name: ', torch.cuda.get_device_name(device=device))
    print('---------------------------')

if use_cuda:
    torch.cuda.empty_cache()

def cuda(tensor, use_cuda):
    return tensor.cuda() if use_cuda else tensor


# Architecture definition. 
class Net_B3(nn.Module):
    def __init__(self, *model_par):
        super(Net_B3, self).__init__()      
        
        x_len, z_ind, z_dep, c1, c2, c1_, c2_, c4, c5 = model_par
    
        self.x_len = x_len
        self.z_ind = z_ind
        self.z_dep = z_dep           
        
        self.c1 = c1
        self.c2 = c2
      
        self.c1_ = c1_
        self.c2_ = c2_
   
        self.c4 = c4
        self.c5 = c5     

        self.encoder1 = nn.Sequential(
                nn.Linear(x_len, c1, bias = True),
                nn.ReLU(True),
                nn.Linear(c1, c2, bias = True),
                nn.ReLU(True),
                nn.Linear(c2, z_ind, bias = True),
                )
        
        
        self.encoder2 = nn.Sequential(
                nn.Linear(x_len, c1_, bias = True),
                nn.ReLU(True),
                nn.Linear(c1_, c2_, bias = True),
                nn.ReLU(True),
                nn.Linear(c2_, z_dep, bias = True),
                )
        
        self.decoder = nn.Sequential(
                nn.Linear(z_ind + z_dep, c4),
                nn.ReLU(True),
                nn.Linear(c4, c5),
                nn.ReLU(True),
                nn.Linear(c5, x_len)
                )
    
    def forward(self, x):
        
        z_ind_ = self.encoder1(x)
        z_dep_ = self.encoder2(x)
        z = torch.cat((z_ind_, z_dep_), dim=1)
        x_ = self.decoder(z)           

        return x_, z


def load_data(dataset_name, fold_idx):
    """
    dataset: name of the dataset, either "adience", "colorferet" or "lfw".
    fold_idx: index of a fold, valid values are: 0,1,2,3,4.
    """
    def binarize_gender_labels(labels):
        return np.array([1 if xi == u"Male" else 0 for xi in labels[:, 1]])

    def colorferet_binarize_gender_labels(labels):
        return labels[:, 1]

    dataset     = np.load(os.path.join("data", dataset_name, "ds_" +dataset_name+ "_facenet_emb.npy"), allow_pickle = True) 
    labels      = np.load(os.path.join("data", dataset_name, "ds_" +dataset_name+ "_facenet_labels.npy"), allow_pickle = True)
    train_idxs  = np.load(os.path.join("data", dataset_name, "folds_" + dataset_name + ".npy"), allow_pickle = True).item()["trainingIdx"][fold_idx]
    test_idxs   = np.array([idx for idx in range(dataset.shape[0]) if idx not in train_idxs])
    
    X_train = np.array(dataset[train_idxs], dtype=np.float32)
    y_train = labels[train_idxs] 

    n_all_train_examples = len(X_train)

    X_train_val = X_train[:int(validation_percent*n_all_train_examples)]
    y_train_val = y_train[:int(validation_percent*n_all_train_examples)]

    X_train = X_train[int(validation_percent*n_all_train_examples):]
    y_train = y_train[int(validation_percent*n_all_train_examples):]
    
    X_test = dataset[test_idxs]
    y_test = labels[test_idxs]

    if dataset_name == "lfw" or dataset_name == "adience":
        y_train     = binarize_gender_labels(y_train)
        y_test      = binarize_gender_labels(y_test)
        y_train_val = binarize_gender_labels(y_train_val)
    elif dataset_name == "colorferet":
        y_train     = colorferet_binarize_gender_labels(y_train)
        y_test      = colorferet_binarize_gender_labels(y_test)
        y_train_val = colorferet_binarize_gender_labels(y_train_val)

    return X_train, y_train, X_train_val, y_train_val, X_test, y_test


def moments_loss(z_ind_m, z_ind_f, z_dep_m, z_dep_f, alpha_max, beta_max):
    
    lam_j = 0.01
    
    loss = 0
    if alpha_max > 0:
        for k in range(1,1 + alpha_max):
            
            Z_ind_m_k = (z_ind_m.pow(k)).sum(dim=0)/z_ind_m.size(0)
            Z_ind_f_k = (z_ind_f.pow(k)).sum(dim=0)/z_ind_f.size(0)
                       
            loss += F.mse_loss(Z_ind_m_k, Z_ind_f_k)
    
    loss_second_term = 0
    if beta_max > 0:
        for j in range(1,1 + beta_max):
            
            Z_dep_m_j = (z_dep_m.pow(j)).sum(dim=0)/z_dep_m.size(0)
            Z_dep_f_j = (z_dep_f.pow(j)).sum(dim=0)/z_dep_f.size(0)
            
            loss_second_term += lam_j*(torch.exp(-(Z_dep_m_j - Z_dep_f_j)**2)).sum()
    
    return loss + loss_second_term


def train(net, X_train, y_train, optimizer, batch_size):
    
    net.train(mode = True)
    sum_ind_loss = 0
    sum_rec_loss = 0

    train_len = len(X_train) 
    iter_max = train_len // batch_size

    for iteration in range(iter_max + 1):
            
        if (iteration+1)*batch_size <= train_len:
            X = X_train[iteration*batch_size : (iteration+1)*batch_size]
            y = y_train[iteration*batch_size : (iteration+1)*batch_size]

        elif (iteration+1) * batch_size < train_len:
            X = X_train[iteration * batch_size:]
            y = y_train[iteration * batch_size:]
        else:
            break

        optimizer.zero_grad()
        X_, Z = net(X)
        Z_ind = Z[:, :z_ind]
        Z_dep = Z[:, z_ind:]

        Z_ind_m = Z_ind[y.bool()]
        Z_ind_f = Z_ind[( (y+1)%2 ).bool() ]
        Z_dep_m = Z_dep[y.bool()]
        Z_dep_f = Z_dep[( (y+1)%2 ).bool() ]

        ind_loss = moments_loss(Z_ind_m, Z_ind_f, Z_dep_m, Z_dep_f, alpha_max, beta_max)
        rec_loss = F.mse_loss(X,X_)

        sum_rec_loss += rec_loss
        sum_ind_loss += ind_loss

        loss = rec_loss + ind_loss
        loss.backward()
        optimizer.step()

    avg_rec_loss = float(sum_rec_loss) / iter_max    
    avg_ind_loss = float(sum_ind_loss) / iter_max
    
    return avg_rec_loss, avg_ind_loss

def validate(net, X_train_val, y_train_val, n_parts): #, batch_size=1):
    net.eval()

    # Split validation set to 'n_parts' parts and calculate average performance
    # on those parts. It is done this way since it needs to be assured that 
    # at least 1 male and 1 female are present in individual part, otherwise
    # loss function gives NaN. 

    val_len = len(X_train_val)
    
    batch_size = int(len(y_train_val) / n_parts)

    sum_rec_loss = 0
    sum_ind_loss = 0
        
    with torch.no_grad():
        
        for iteration in range(n_parts):
            
            if (iteration+1)*batch_size <= val_len:
                X = X_train_val[iteration*batch_size : (iteration+1)*batch_size]
                y = y_train_val[iteration*batch_size : (iteration+1)*batch_size]
                      
            elif iteration * batch_size < val_len:
                X = X_train_val[iteration*batch_size:]
                y = y_train_val[iteration*batch_size:]
            else:
                break
            
            X_, Z = net(X)
            Z_ind = Z[:, :z_ind]
            Z_dep = Z[:, z_ind:]
       
            Z_ind_m = Z_ind[y.bool()]
            Z_ind_f = Z_ind[( (y+1)%2 ).bool() ]
            Z_dep_m = Z_dep[y.bool()]
            Z_dep_f = Z_dep[( (y+1)%2 ).bool() ]
            
            ind_loss = moments_loss(Z_ind_m, Z_ind_f, Z_dep_m, Z_dep_f, alpha_max, beta_max)
            rec_loss = F.mse_loss(X,X_)

            sum_rec_loss += rec_loss
            sum_ind_loss += ind_loss
    
    avg_rec_loss = float(sum_rec_loss) / n_parts
    avg_ind_loss = float(sum_ind_loss) / n_parts
            
    return avg_rec_loss, avg_ind_loss


def get_z_vectors(net, X_test):
    #X_test = cuda(torch.from_numpy(np.array(X_test, dtype=np.float32)), use_cuda)
    net.eval()
    with torch.no_grad():
        data_length = len(X_test)

        Z = np.empty((data_length, 128))
        
        for iteration in range(data_length): 
            x = torch.unsqueeze(X_test[iteration], 0)#, axis=0)
            x_, z = net(x)
            Z[iteration] = z.cpu().detach().numpy()# * batch_size : (iteration+1)*batch_size] = 
        return Z


def create_directory(path):
    if not os.path.exists(path):
        os.mkdir(path)


def tsne_visualize(X, y, n_samples_per_gender, figure_title):

    male_idxs = np.nonzero(y)[0]
    female_idxs = np.where(y == 0)[0]

    # If there are no n_samples_per_gender available samples, 
    # take all samples that you can.
    if len(female_idxs) < n_samples_per_gender:
        print("Warning: There were not enough female samples (requested "
            + str(n_samples_per_gender)
            + "), so we took only " 
            + str(len(female_idxs))
            + " of female samples.")
        n_samples_per_gender = len(female_idxs)

    if len(male_idxs) < n_samples_per_gender:
        print("Warning: There were not enough male samples (requested " 
            + str(n_samples_per_gender)
            + "), so we took only " 
            + str(len(male_idxs))
            + " of male samples.")
        n_samples_per_gender = len(male_idxs)
    
    # Assure that the split was ok
    assert np.intersect1d(male_idxs, female_idxs).shape[0] == 0
    assert np.union1d(male_idxs, female_idxs).shape[0] == len(y)
    
    # Get indexes of random n samples for each gender
    sampled_male_idxs = np.random.choice(male_idxs, n_samples_per_gender)
    sampled_female_idxs = np.random.choice(female_idxs, n_samples_per_gender)

    # Obtain samples
    male_samples   = X[sampled_male_idxs]
    female_samples = X[sampled_female_idxs]
    
    X_subsampled = np.concatenate((male_samples, male_samples), axis=0)
    X_emb = TSNE(n_components = 2).fit_transform(X_subsampled)

    axis_1 = X_emb.T[0]
    axis_2 = X_emb.T[1]

    fsl = 10
    plt.figure(figsize =(4,3))
    plt.scatter(axis_1[:n_samples_per_gender], axis_2[:n_samples_per_gender], c ='r', s = 5, label = r'female')
    plt.scatter(axis_1[n_samples_per_gender:], axis_2[n_samples_per_gender:], c ='b', s = 5, label = r'male')
    plt.xlabel('dimension 1', fontsize = fsl)
    plt.ylabel('dimension 2', fontsize = fsl)
    plt.title(figure_title, fontsize = fsl)
    plt.legend(fontsize = fsl)
    plt.tight_layout()
    plt.axis('equal')
    plt.show()


if __name__ == "__main__": 

    create_directory("models")
    create_directory("z_vectors")

    # We're using three layer network.
    x_len = 128
    c1  = 128
    c2  = 128
    c1_ = 128
    c2_ = 128
    c4  = 128 
    c5  = 128
    z_ind = 100
    z_dep = 28
    model_par = [x_len, z_ind, z_dep, c1, c2, c1_, c2_, c4, c5]

    # Training hyperparams. 
    batch_size = 2000
    n_folds = 5
    n_epochs = 5000
    lr = 0.0001
    alpha_max = 2 
    beta_max = 2
    validation_percent = 0.2
    dataset_name = "adience"

    # Evaluation frequency.
    validate_after_this_n_of_epochs = 10
    n_validation_parts = 10

    for fold in range(n_folds):
        print("--> [Fold "+str(fold)+"]")
        net = cuda(Net_B3(*model_par), use_cuda) 
        optimizer = torch.optim.Adam(net.parameters(), lr=lr, betas=(0.9, 0.999), eps=1e-8)

        X_train, y_train, X_train_val, y_train_val, X_test, y_test = load_data(dataset_name, fold)
        tsne_visualize(X_train, y_train, 1000, "X")

        # Setup data structure for shuffling. 
        normal_indexes  = np.array(range(len(X_train)))
        shuffle_indexes = np.array(range(len(X_train)))
        
        # Load data to cuda tensors.
        X_train     = cuda(torch.tensor(X_train,     dtype=torch.float), use_cuda)
        y_train     = cuda(torch.tensor(y_train,     dtype=torch.float), use_cuda)
        X_train_val = cuda(torch.tensor(X_train_val, dtype=torch.float), use_cuda)
        y_train_val = cuda(torch.tensor(y_train_val, dtype=torch.float), use_cuda)
        X_test = cuda(torch.tensor(X_test, dtype=torch.float), use_cuda)
               
        best_validation_loss = float('inf')
        best_model_weights = None
        for epoch in range(n_epochs):
            
            # Shuffle only training data
            np.random.shuffle(shuffle_indexes)
            X_train[normal_indexes] = X_train[shuffle_indexes]
            y_train[normal_indexes] = y_train[shuffle_indexes]

            rec_loss, ind_loss = train(net, X_train, y_train, optimizer, batch_size)
            print('epoch: %s train_loss: (rec_loss: %0.5f, ind_loss: %0.5f)'%(epoch, float(rec_loss), float(ind_loss)))

            if epoch % validate_after_this_n_of_epochs == 0:
                print("[Validation phase]")
                rec_validation_loss, ind_validation_loss = validate(net, X_train_val, y_train_val, n_validation_parts)#, batch_size)
                print('epoch: %s validation_loss: (rec_loss: %0.5f, ind_loss: %0.5f)'%(epoch, float(rec_validation_loss), float(ind_validation_loss)))
                validation_loss = rec_validation_loss + ind_validation_loss
                if best_validation_loss > validation_loss:
                    best_validation_loss = validation_loss
                    print("best_validation_loss : %0.7f   validation_loss: %0.7f" % (float(best_validation_loss), float(validation_loss)))
                    best_model_weights = net.state_dict()
                    torch.save(net.state_dict(), os.path.join("models", "dataset_" + dataset_name + "_fold_" + str(fold)+ ".pth"))

        # Export vectors z, with the best model. 
        net.load_state_dict(best_model_weights)

        print("Obtaining and saving z_vectors.")
        Z = get_z_vectors(net, X_test)#, batch_size)

        Z_ind = Z[:, :z_ind]
        Z_dep = Z[:, z_ind:]
        tsne_visualize(Z_ind, y_test, 1000, "Z_ind")
        tsne_visualize(Z_dep, y_test, 1000, "Z_dep")

        np.save(os.path.join("z_vectors", "Z_dataset_" + dataset_name + "_fold_" + str(fold) + ".pth"), Z)
